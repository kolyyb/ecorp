import mysql.connector as msql
import csv


with open("recipes.csv", 'r') as file:
    reader = csv.reader(file)

    conn = msql.connect(host='localhost', user='root', password='pass', database='ecorp')

    cursor = conn.cursor()

    for row in reader:
        print(row)
        cursor.execute("""INSERT INTO temp_recipes 
        (ingredient, spoon, name)
            VALUES (%s, %s, %s);""", row)
        conn.commit()
    cursor.close()
