import mysql.connector as msql
from fastapi import FastAPI

from models import Ingredient


conn = msql.connect(host='localhost', user='root', password='pass', database='ecorp')

app = FastAPI()


@app.get('/ingredient/{name}')
def show_ingredient_by_name(name: str) -> list:
    dict_cursor = conn.cursor(dictionary=True)
    query = """SELECT ingredient_name, restaurant_name, ingredient_quantity, sum(ingredient_quantity) as 'total sum'
                FROM stocks
                JOIN ingredients ON stocks.id_ingredient = ingredients.id
                JOIN restaurants ON stocks.id_restaurant = restaurants.id
        WHERE ingredient_name = %s
            GROUP BY(restaurant_name) WITH ROLLUP ;"""
    dict_cursor.execute(query, (name,))
    ingredient_by_name = dict_cursor.fetchall()
    return ingredient_by_name


@app.get('/recipe/{name}')
def show_recipe_by_name(name: str):
    dict_cursor = conn.cursor(dictionary=True)
    query = "SELECT recipes_name FROM recipes WHERE recipes_name = %s;"
    dict_cursor.execute(query, (name,))
    recipe_by_name = dict_cursor.fetchall()
    return recipe_by_name


@app.post('/ingredient/add')
def add_ingredient(ingredient: Ingredient):
    cursor = conn.cursor()
    query = "INSERT INTO ingredients (ingredient_name) VALUES (%s);"
    val = ingredient.ingredient_name
    cursor.execute(query, (val,))
    conn.commit()
    ingredient.id = cursor.lastrowid
    return ingredient

