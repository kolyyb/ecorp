CREATE TABLE ingredients
(
    id              INT PRIMARY KEY AUTO_INCREMENT,
    ingredient_name VARCHAR(30)
);

INSERT INTO ingredients (ingredient_name)
VALUES ('dopamine');

CREATE TABLE stocks
(
    id                  INT PRIMARY KEY AUTO_INCREMENT,
    ingredient_quantity INT,
    id_ingredient       INT,
    id_restaurant       INT,
    date                DATETIME,
    FOREIGN KEY (id_ingredient) REFERENCES ingredients (id),
    FOREIGN KEY (id_restaurant) REFERENCES restaurants (id)
);

CREATE TABLE recipes
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    recipes_name VARCHAR(50)
);

CREATE TABLE ingredients_recipes
(
    quantity      INT,
    id_recipes    INT,
    id_ingredient INT,
    FOREIGN KEY (id_recipes) REFERENCES recipes (id),
    FOREIGN KEY (id_ingredient) REFERENCES ingredients (id)
);

CREATE TABLE ingredient_units
(
    id        INT PRIMARY KEY AUTO_INCREMENT,
    unit_name VARCHAR(10)
);

ALTER TABLE ingredients_recipes
    add (id_unit INT,
         FOREIGN KEY (id_unit) REFERENCES ingredient_units (id));

ALTER TABLE stocks
    add (id_unit INT,
         FOREIGN KEY (id_unit) REFERENCES ingredient_units (id));

-- Extract stock_ingredients.ingredient_name copy to ingrediengds.ingredient_name (no duplicate items)
INSERT INTO ingredients (ingredient_name)
    (SELECT DISTINCT (ingredient_name) FROM stock_ingredients ORDER BY ingredient_name ASC);

/* check good relations between tables before creation
SELECT stock_ingredients.ingredient_name,
        ingredients.id AS ingredient_id,
        stock_ingredients.ingredient_quantity,
        stock_ingredients.id_restaurant
    FROM stock_ingredients, ingredients
WHERE stock_ingredients.ingredient_name = ingredients.ingredient_name;*/

INSERT INTO stocks (id_ingredient, ingredient_quantity, id_restaurant, date)
SELECT ingredients.id,
       stock_ingredients.ingredient_quantity,
       stock_ingredients.id_restaurant,
       stock_ingredients.date
FROM stock_ingredients,
     ingredients
WHERE stock_ingredients.ingredient_name = ingredients.ingredient_name;

CREATE TABLE temp_recipes
(
    ingredient VARCHAR(30),
    spoon      INT,
    name       VARCHAR(50)
);

ALTER TABLE ingredients
    ADD
        CONSTRAINT UQ_ingredient_name UNIQUE (ingredient_name);

INSERT INTO recipes (recipes_name)
SELECT DISTINCT (name)
FROM temp_recipes;
--
SELECT ingredients.ingredient_name, ingredients.id, ingredient, spoon, name
FROM temp_recipes
         LEFT JOIN ingredients on ingredients.ingredient_name = temp_recipes.ingredient;


SELECT temp_recipes.ingredient, ingredients.id, temp_recipes.spoon, recipes.id, temp_recipes.name
FROM temp_recipes
         JOIN ingredients ON ingredients.ingredient_name = temp_recipes.ingredient
         JOIN recipes ON recipes.recipes_name = temp_recipes.name;


INSERT INTO ingredients_recipes (quantity, id_recipes, id_ingredient)
SELECT temp_recipes.spoon, recipes.id, ingredients.id
FROM temp_recipes
         JOIN ingredients ON ingredients.ingredient_name = temp_recipes.ingredient
         JOIN recipes ON recipes.recipes_name = temp_recipes.name;

SELECT ingredients_recipes.quantity, recipes.recipes_name, ingredients.ingredient_name
FROM ingredients_recipes
         JOIN ingredients ON ingredients.id = ingredients_recipes.id_ingredient
         JOIN recipes ON recipes.id = ingredients_recipes.id_recipes;

INSERT INTO ingredient_units (unit_name)
VALUES ('g'),
       ('ml');

SELECT ingredient_name, restaurant_name, ingredient_quantity, sum(ingredient_quantity) as 'total sum'
FROM stocks
         JOIN ingredients ON stocks.id_ingredient = ingredients.id
         JOIN restaurants ON stocks.id_restaurant = restaurants.id
WHERE ingredient_name = 'amande poudre'
GROUP BY(restaurant_name) WITH ROLLUP ;
