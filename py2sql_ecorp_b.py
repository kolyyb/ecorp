from datetime import date

import mysql.connector as msql
import csv

with open("ecorpB.csv", 'r') as file:
    reader = csv.reader(file)

    conn = msql.connect(host='localhost', user='root', password='pass', database='ecorp')

    cursor = conn.cursor()

    for row in reader:
        # print(row)

        cursor.execute("""INSERT INTO stock_ingredients 
        (ingredient_name, date, ingredient_quantity, id_restaurant)
            VALUES (%s, %s, %s, %s);""", (row[1],  date.fromisoformat(row[2]), row[3], row[4]))
        conn.commit()
    cursor.close()
