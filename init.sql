DROP DATABASE IF EXISTS ecorp;
CREATE DATABASE ecorp;
USE ecorp;

CREATE TABLE restaurants (id INT PRIMARY KEY AUTO_INCREMENT, restaurant_name VARCHAR(30));

CREATE TABLE stock_ingredients (
    id INT PRIMARY KEY AUTO_INCREMENT, ingredient_name VARCHAR(30), ingredient_quantity INT, date DATETIME, id_restaurant INT,
    FOREIGN KEY (id_restaurant) REFERENCES restaurants(id));

INSERT INTO restaurants (restaurant_name)
VALUES
    ("EcorpA"),
    ("EcorpB"),
    ("EcorpC");