import mysql.connector as msql
import csv


with open("ecorpC.csv", 'r') as file:
    reader = csv.reader(file)

    conn = msql.connect(host='localhost', user='root', password='pass', database='ecorp')

    cursor = conn.cursor()

    for row in reader:
        # print(row)
        cursor.execute("""INSERT INTO stock_ingredients 
        (ingredient_name, id_restaurant, ingredient_quantity)
            VALUES (%s, %s, %s);""", row)
        conn.commit()
    cursor.close()
